import React, { Component, Fragment } from "react";

class RobotForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "",
      name: "",
      mass: 0
    };
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <Fragment>
        <h3>Add a new robot</h3>
        <div>
          <label htmlFor='type'>
            Robot type:
            <input
              type='text'
              name='type'
              id='type'
              placeholder='worker'
              required
              value={this.state.type}
              onChange={this.onChange}
            />
          </label>
        </div>
        <div>
          <label htmlFor='name'>
            Robot name:
            <input
              type='text'
              name='name'
              id='name'
              placeholder='Robotzelu'
              value={this.state.name}
              onChange={this.onChange}
            />
          </label>
        </div>

        <div>
          <label htmlFor='mass'>
            Robot mass:
            <input
              type='number'
              name='mass'
              id='mass'
              placeholder='1000'
              value={this.state.mass}
              onChange={this.onChange}
            />
          </label>
        </div>

        <input
          type='submit'
          value='add'
          onClick={() => this.props.onAdd(this.state)}
        />
      </Fragment>
    );
  }
}

export default RobotForm;
