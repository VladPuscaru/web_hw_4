import React, { Component } from "react";
import RobotStore from "../stores/RobotStore";
import Robot from "./Robot";
import RobotForm from "./RobotForm";

class RobotList extends Component {
  constructor() {
    super();
    this.state = {
      robots: []
    };

    this.store = new RobotStore();
    this.store.emitter.addListener("UPDATE", () => {
      this.setState({
        robots: this.store.getRobots()
      });
    });
  }
  componentDidMount() {
    this.setState({
      robots: this.store.getRobots()
    });
  }

  onAdd = r => {
    this.store.addRobot(r);
  };

  render() {
    return (
      <div>
        {this.state.robots.map((e, i) => (
          <Robot item={e} key={i} />
        ))}

        <RobotForm onAdd={this.onAdd} />
      </div>
    );
  }
}

export default RobotList;
